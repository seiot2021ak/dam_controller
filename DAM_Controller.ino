#include <SoftwareSerial.h>
#include <Servo.h>
#include "src/Led.h"
#include "src/MsgService.h"

#define PIN_SERVO 5
#define PIN_LED 4
#define PIN_BT_TXD 2
#define PIN_BT_RXD 3
#define PIN_BT_KEY 10
#define PIN_BT_STATE 11

#define TIMER_RESET 10

/*
   Level of system alert.
   0 = normal
   1 = pre-alarm
   2 = alarm
*/
int alarmStatus;
/*
   Manual control flag.
*/
boolean manualControl;
/*
   Percentage opening of dam.
   Autorized values:
   0, 20, 40, 60, 80, 100
*/
int openingPerc;
/*
   Vector for communication transmission.
*/
String message;

SoftwareSerial BTComm(PIN_BT_TXD, PIN_BT_RXD);
Servo flowValve;
Led statusLED(PIN_LED);
boolean activeLedFlag;
int activeLedTimer;

// ---------
//   SETUP
// ---------
void setup()
{
  alarmStatus = 0;
  openingPerc = 0;
  manualControl = false;

  statusLED.switchOff();
  activeLedFlag = false;
  activeLedTimer = TIMER_RESET;

  flowValve.attach(PIN_SERVO, 0, 180);
  flowValve.write(openingPerc);

  Serial.begin(9600);
  BTComm.begin(9600);
  MsgService.init();
}

// --------
//   LOOP
// --------
void loop()
{
  message = collectSerialMessage();
  decode(message, 0);
  message = collectBTMessage();
  decode(message, 1);

  checkStatusLED();

  delay(50);
}

// -----------
//   UTILITY
// -----------

/**
   Collect message received from Serial.
   Decode and send to Bluetooth.
*/
String collectSerialMessage() {
  if (MsgService.isMsgAvailable()) {

    Msg* m = MsgService.receiveMsg();
    String s = m->getContent()+"\n";
    BTComm.write(s.c_str());

    delete m;
    s.trim();

    return s;
  }
  return "";
}

/**
   Collect message received from Bluetooth.
   Decode and send to Serial.
*/
String collectBTMessage() {
  if (BTComm.available()) {
    String s;
    while(BTComm.available()>0){
      s+=String((char)BTComm.read());
    }
    s.trim();
    MsgService.sendMsg(s);
    return s;
  }
  return "";
}

/**
   Decode message received.
   Analyze message type.
*/
void decode(String s, int channel) {
  String msgType = s.substring(0, s.indexOf(":"));
  String msgContent = s.substring(s.indexOf(":") + 1);

  if (msgType.equals("INFO")) {  }

  if (msgType.equals("DEBUG")) { 
    const String label = "DEBUG|";
    if(msgContent.equals("MANUAL")){
      Serial.println(label+String(manualControl));
    }
    if(msgContent.equals("PERCENTAGE")){
      Serial.println(label+String(openingPerc));
    }
    if(msgContent.equals("ALERT")){
      Serial.println(label+String(alarmStatus));
    }
    if(msgContent.equals("ALL")){
      Serial.println(label+"ALERT:"+String(alarmStatus)+", MANUAL:"+String(manualControl)+", PERCENTAGE:"+String(openingPerc));
    }
  }

  if (msgType.equals("MANUAL")) {    // MANUAL: manual control option
    if (alarmStatus == 2) {
      if (msgContent.equals("TRUE")) {
        manualControl = true;
      } else if (msgContent.equals("FALSE")) {
        manualControl = false;
      }
    }
  }

  if (msgType.equals("PERCENTAGE")) {   // PERCENTAGE: percent value of dam opening
    if (channel == 0) {  //serial
      if (!manualControl) {
        openingPerc = (int) msgContent.toInt();
        int open = openingPerc * 180 / 100;
        flowValve.write(open);
      }
    }
    if (channel == 1) {  //bluetooth
      if (manualControl) {
        openingPerc = (int) msgContent.toInt();
        int open = openingPerc * 180 / 100;
        flowValve.write(open);
      }
    }
  }

  if (msgType.equals("ALERT")) {    // ALERT: change level of system alert
    if (msgContent.equals("NORMAL")) {
      alarmStatus = 0;
    } else if (msgContent.equals("PRE-ALARM")) {
      alarmStatus = 1;
    } else if (msgContent.equals("ALARM")) {
      alarmStatus = 2;
    }
  }

  if (msgType.equals("WATER")) {  }

}

void checkStatusLED() {
  
  if (manualControl){
    statusLED.switchOn();
  } else if (alarmStatus == 2) {    // ALARM
    //check flag to switch led
    if (activeLedFlag) {
      statusLED.switchOn();
    } else {
      statusLED.switchOff();
    }
    
    //decrease timer
    activeLedTimer--;
    
    //check if timer is over and reset
    if (activeLedTimer <= 0) {
      if (activeLedFlag) {
        activeLedFlag = false;
      } else {
        activeLedFlag = true;
      }
      activeLedTimer = TIMER_RESET;
    }

  } else {
    
    statusLED.switchOff();

  }
}
